<?php

/* index.twig */
class __TwigTemplate_84da88a59d69dbec399a9e40f88dc921ec5f6dc9b0f69d6f7f19432d1cfd1726 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return $this->loadTemplate(($context["layout"] ?? null), "index.twig", 1);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "
    <div class=\"uk-section-slider\">
        <div class=\"uk-position-relative uk-visible-toggle uk-light\" uk-slideshow=\"animation: fade; autoplay: true; autoplay-interval: 5000; min-height: 200; max-height: 400;\">
            <ul class=\"uk-slideshow-items\">
                <li>
                    <img src=\"http://blizzcms.v2/assets/slides/slide1.jpg\" alt=\"\" uk-cover>
                    <div class=\"uk-position-center-left uk-position-medium uk-text-center uk-light\">
                        <h2 class=\"uk-margin-medium-left\">BlizzCMS</h2>
                    </div>
                </li>
                <li>
                    <img src=\"http://blizzcms.v2/assets/slides/slide2.jpg\" alt=\"\" uk-cover>
                    <div class=\"uk-position-center-left uk-position-medium uk-text-center uk-light\">
                        <h2 class=\"uk-margin-medium-left\">BlizzCMS</h2>
                    </div>
                </li>
            </ul>
            <a class=\"uk-position-center-left uk-position-small uk-hidden-hover\" href=\"#\" uk-slidenav-previous uk-slideshow-item=\"previous\"></a>
            <a class=\"uk-position-center-right uk-position-small uk-hidden-hover\" href=\"#\" uk-slidenav-next uk-slideshow-item=\"next\"></a>
        </div>
    </div>
    <div class=\"uk-section-xsmall\">
        <div class=\"uk-container\">
            <div class=\"uk-grid uk-grid-medium\" data-uk-grid>
                <div class=\"uk-width-2-3@m\">

\t\t\t\t\t<section id=\"all_news\">
\t\t\t\t\t\t<div class=\"uk-card uk-card-default uk-margin\" v-for=\"allnews in allnews\">
\t\t\t\t\t\t\t<div class=\"uk-card-header\">
\t\t\t\t\t\t\t\t<div class=\"uk-grid-small uk-flex-middle\" uk-grid>
\t\t\t\t\t\t\t\t\t<div class=\"uk-width-expand\">
\t\t\t\t\t\t\t\t\t\t<h3 class=\"uk-card-title uk-margin-remove-bottom\">";
        // line 35
        echo "{{ allnews.title }}";
        echo "</h3>
\t\t\t\t\t\t\t\t\t\t<p class=\"uk-text-meta uk-margin-remove-top\"><time datetime=\"2018-04-06T19:00\">";
        // line 36
        echo "{{ allnews.date }}";
        echo "</time></p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"uk-card-body\">
\t\t\t\t\t\t\t\t<p>";
        // line 41
        echo "{{ allnews.description }}";
        echo "</p>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"uk-card-footer\">
\t\t\t\t\t\t\t\t<a href=\"#\" class=\"uk-button uk-button-primary uk-align-right\">";
        // line 44
        echo twig_escape_filter($this->env, ($context["readmore"] ?? null), "html", null, true);
        echo "</a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</section>

                    <ul class=\"uk-pagination uk-flex-center\" uk-margin>
                        <li><a href=\"#\"><span uk-pagination-previous></span></a></li>
                        <li><a href=\"#\">1</a></li>
                        <li class=\"uk-disabled\"><span>...</span></li>
                        <li><a href=\"#\">5</a></li>
                        <li><a href=\"#\">6</a></li>
                        <li class=\"uk-active\"><span>7</span></li>
                        <li><a href=\"#\">8</a></li>
                        <li><a href=\"#\"><span uk-pagination-next></span></a></li>
                    </ul>
                </div>
                <div class=\"uk-width-1-3@m\">
                    <div class=\"uk-card uk-card-default uk-margin\">
                        <div class=\"uk-card-header\">
                            <h4 class=\"uk-card-title uk-margin-remove-bottom uk-text-center\"><i class=\"fas fa-server\"></i> ";
        // line 63
        echo twig_escape_filter($this->env, ($context["status"] ?? null), "html", null, true);
        echo "</h4>
                        </div>
                        <div class=\"uk-card-body\">
                            <p><i class=\"fas fa-gamepad\"></i> ";
        // line 66
        echo twig_escape_filter($this->env, ($context["realmlist"] ?? null), "html", null, true);
        echo "</p>
                        </div>
                    </div>
                    <div class=\"uk-card uk-card-default uk-margin\">
                        <div class=\"uk-card-header\">
                            <h4 class=\"uk-card-title uk-margin-remove-bottom uk-text-center\"><i class=\"fab fa-discord\"></i> Discord</h4>
                        </div>
                        <div class=\"uk-card-body\">
                            <p></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

\t<script type=\"text/javascript\">
\t\tvar urlNews = \"";
        // line 83
        echo twig_escape_filter($this->env, base_url("news/apinews"), "html", null, true);
        echo "\";
\t\tnew Vue({
\t\t\tel: '#all_news',
\t\t\tcreated: function(){
\t\t\t\tthis.getNews();
\t\t\t},
\t\t\tdata:{
\t\t\t\tallnews: [],
\t\t\t},
\t\t\tmethods:{
\t\t\t\tgetNews: function(){
\t\t\t\t\tthis.\$http.get(urlNews).then(function(response) {
\t\t\t\t\t\tthis.allnews = response.data;
\t\t\t\t\t});
\t\t\t\t}
\t\t\t}
\t\t});
\t</script>

";
    }

    public function getTemplateName()
    {
        return "index.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  129 => 83,  109 => 66,  103 => 63,  81 => 44,  75 => 41,  67 => 36,  63 => 35,  30 => 4,  27 => 3,  18 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends layout %}

{% block content %}

    <div class=\"uk-section-slider\">
        <div class=\"uk-position-relative uk-visible-toggle uk-light\" uk-slideshow=\"animation: fade; autoplay: true; autoplay-interval: 5000; min-height: 200; max-height: 400;\">
            <ul class=\"uk-slideshow-items\">
                <li>
                    <img src=\"http://blizzcms.v2/assets/slides/slide1.jpg\" alt=\"\" uk-cover>
                    <div class=\"uk-position-center-left uk-position-medium uk-text-center uk-light\">
                        <h2 class=\"uk-margin-medium-left\">BlizzCMS</h2>
                    </div>
                </li>
                <li>
                    <img src=\"http://blizzcms.v2/assets/slides/slide2.jpg\" alt=\"\" uk-cover>
                    <div class=\"uk-position-center-left uk-position-medium uk-text-center uk-light\">
                        <h2 class=\"uk-margin-medium-left\">BlizzCMS</h2>
                    </div>
                </li>
            </ul>
            <a class=\"uk-position-center-left uk-position-small uk-hidden-hover\" href=\"#\" uk-slidenav-previous uk-slideshow-item=\"previous\"></a>
            <a class=\"uk-position-center-right uk-position-small uk-hidden-hover\" href=\"#\" uk-slidenav-next uk-slideshow-item=\"next\"></a>
        </div>
    </div>
    <div class=\"uk-section-xsmall\">
        <div class=\"uk-container\">
            <div class=\"uk-grid uk-grid-medium\" data-uk-grid>
                <div class=\"uk-width-2-3@m\">

\t\t\t\t\t<section id=\"all_news\">
\t\t\t\t\t\t<div class=\"uk-card uk-card-default uk-margin\" v-for=\"allnews in allnews\">
\t\t\t\t\t\t\t<div class=\"uk-card-header\">
\t\t\t\t\t\t\t\t<div class=\"uk-grid-small uk-flex-middle\" uk-grid>
\t\t\t\t\t\t\t\t\t<div class=\"uk-width-expand\">
\t\t\t\t\t\t\t\t\t\t<h3 class=\"uk-card-title uk-margin-remove-bottom\">{{ \"{{ allnews.title }}\" }}</h3>
\t\t\t\t\t\t\t\t\t\t<p class=\"uk-text-meta uk-margin-remove-top\"><time datetime=\"2018-04-06T19:00\">{{ \"{{ allnews.date }}\" }}</time></p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"uk-card-body\">
\t\t\t\t\t\t\t\t<p>{{ \"{{ allnews.description }}\" }}</p>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"uk-card-footer\">
\t\t\t\t\t\t\t\t<a href=\"#\" class=\"uk-button uk-button-primary uk-align-right\">{{ readmore }}</a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</section>

                    <ul class=\"uk-pagination uk-flex-center\" uk-margin>
                        <li><a href=\"#\"><span uk-pagination-previous></span></a></li>
                        <li><a href=\"#\">1</a></li>
                        <li class=\"uk-disabled\"><span>...</span></li>
                        <li><a href=\"#\">5</a></li>
                        <li><a href=\"#\">6</a></li>
                        <li class=\"uk-active\"><span>7</span></li>
                        <li><a href=\"#\">8</a></li>
                        <li><a href=\"#\"><span uk-pagination-next></span></a></li>
                    </ul>
                </div>
                <div class=\"uk-width-1-3@m\">
                    <div class=\"uk-card uk-card-default uk-margin\">
                        <div class=\"uk-card-header\">
                            <h4 class=\"uk-card-title uk-margin-remove-bottom uk-text-center\"><i class=\"fas fa-server\"></i> {{ status }}</h4>
                        </div>
                        <div class=\"uk-card-body\">
                            <p><i class=\"fas fa-gamepad\"></i> {{ realmlist }}</p>
                        </div>
                    </div>
                    <div class=\"uk-card uk-card-default uk-margin\">
                        <div class=\"uk-card-header\">
                            <h4 class=\"uk-card-title uk-margin-remove-bottom uk-text-center\"><i class=\"fab fa-discord\"></i> Discord</h4>
                        </div>
                        <div class=\"uk-card-body\">
                            <p></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

\t<script type=\"text/javascript\">
\t\tvar urlNews = \"{{ base_url('news/apinews') }}\";
\t\tnew Vue({
\t\t\tel: '#all_news',
\t\t\tcreated: function(){
\t\t\t\tthis.getNews();
\t\t\t},
\t\t\tdata:{
\t\t\t\tallnews: [],
\t\t\t},
\t\t\tmethods:{
\t\t\t\tgetNews: function(){
\t\t\t\t\tthis.\$http.get(urlNews).then(function(response) {
\t\t\t\t\t\tthis.allnews = response.data;
\t\t\t\t\t});
\t\t\t\t}
\t\t\t}
\t\t});
\t</script>

{% endblock %}
", "index.twig", "C:\\xampp\\htdocs\\BlizzCMSv2\\application\\modules\\home\\views\\index.twig");
    }
}
