<?php

/* general/layout.twig */
class __TwigTemplate_872eea5bef309975619c41d306c8f6d77c6d15ecd15d52e61529fe1b22fe633a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <meta charset=\"UTF-8\">
    <title>";
        // line 5
        echo twig_escape_filter($this->env, ($context["title"] ?? null), "html", null, true);
        echo "</title>
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, base_url("assets/images/favicon.ico"), "html", null, true);
        echo "\">
    ";
        // line 9
        echo "    <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, base_url("node_modules/rpg-awesome/css/rpg-awesome.min.css"), "html", null, true);
        echo "\" />
    ";
        // line 11
        echo "    <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, base_url("node_modules/uikit/dist/css/uikit.min.css"), "html", null, true);
        echo "\" />
    <script src=\"";
        // line 12
        echo twig_escape_filter($this->env, base_url("node_modules/uikit/dist/js/uikit.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 13
        echo twig_escape_filter($this->env, base_url("node_modules/uikit/dist/js/uikit-icons.min.js"), "html", null, true);
        echo "\"></script>
    ";
        // line 15
        echo "    <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, base_url("vendor/blizzcms/fxgeneral/css/cms.css"), "html", null, true);
        echo "\" />
    ";
        // line 17
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, base_url("node_modules/jquery/dist/jquery.min.js"), "html", null, true);
        echo "\"></script>
    ";
        // line 19
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, base_url("node_modules/vue/dist/vue.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 20
        echo twig_escape_filter($this->env, base_url("node_modules/vue-resource/dist/vue-resource.min.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 21
        echo twig_escape_filter($this->env, base_url("node_modules/vue-moment/vue-moment.js"), "html", null, true);
        echo "\"></script>
    ";
        // line 23
        echo "    <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, base_url("node_modules/cookieconsent/build/cookieconsent.min.css"), "html", null, true);
        echo "\" />
    ";
        // line 25
        echo "    <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, base_url("vendor/blizzcms/amarantjs/css/amaran.min.css"), "html", null, true);
        echo "\" />
    <link rel=\"stylesheet\" href=\"";
        // line 26
        echo twig_escape_filter($this->env, base_url("vendor/blizzcms/amarantjs/css/animate.min.css"), "html", null, true);
        echo "\" />
    ";
        // line 28
        echo "    <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, base_url("vendor/blizzcms/font-awesome/css/fontawesome-all.min.css"), "html", null, true);
        echo "\" />
    ";
        // line 30
        echo "    <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, base_url("vendor/blizzcms/pace/css/pace-theme-minimal.tmpl.css"), "html", null, true);
        echo "\" />
</head>
<body>

    <nav class=\"uk-navbar-container uk-background-primary\">
        <div class=\"uk-container\">
            <div class=\"uk-navbar\" uk-navbar=\"mode: click\">
                <div class=\"uk-navbar-left\">
                    <a class=\"uk-navbar-item uk-logo\" href=\"\">BlizzCMSv2</a>
                </div>
                <div class=\"uk-navbar-center\">
                    <ul class=\"uk-navbar-nav uk-visible@m\">
                        <li>
                            <a href=\"javascript:void(0)\">Menu<span uk-icon=\"icon: chevron-down\"></span></a>
                            <div class=\"uk-navbar-dropdown\">
                                <ul class=\"uk-nav uk-navbar-dropdown-nav\">
                                    <li><a href=\"#\">Item</a></li>
                                    <li><a href=\"#\">Item</a></li>
                                    <li><a href=\"#\">Item</a></li>
                                </ul>
                            </div>
                        </li>
                        <li><a href=\"#\">Forum</a></li>
                        <li><a href=\"#\">Store</a></li>
                    </ul>
                </div>
                <div class=\"uk-navbar-right\">
                    <div class=\"uk-navbar-item uk-visible@m\">
                        <ul class=\"uk-grid-small\" uk-grid>
                            <li>
                                <a href=\"\" class=\"uk-button uk-button-primary\"><span uk-icon=\"icon: sign-in\"></span> Sign In</a>
                            </li>
                        </ul>
                    </div>
                    <a class=\"uk-navbar-toggle uk-hidden@m\" uk-navbar-toggle-icon href=\"#mobile\" uk-toggle></a>
                </div>
                ";
        // line 67
        echo "                <div class=\"uk-offcanvas-content\">
                    <div id=\"mobile\" uk-offcanvas=\"flip: true\">
                        <div class=\"uk-offcanvas-bar\">
                            <div class=\"uk-panel\">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>

    ";
        // line 79
        $this->displayBlock('content', $context, $blocks);
        // line 80
        echo "
    <div class=\"uk-section uk-section-small\">
        <div class=\"uk-container\">
            <div class=\"uk-grid-large uk-flex-middle uk-grid\" uk-grid>
                <div class=\"uk-width-expand uk-first-column\">
                    <p class=\"uk-align-left\"><a target=\"_blank\" href=\"https://gitlab.com/BlizzCMS\" class=\"uk-button uk-button-link\"><i class=\"fab fa-gitlab fa-2x\"></i></a> Proudly powered by <span class=\"uk-text-bold\">BlizzCMS</span></p>
                </div>
                <div class=\"uk-width-auto\">
                    <p>Copyright &copy; 2018 <span class=\"uk-text-bold\">BlizzCMSv2</span>. All Right Reserved.</p>
                </div>
            </div>
        </div>
    </div>

    ";
        // line 95
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, base_url("node_modules/cookieconsent/build/cookieconsent.min.js"), "html", null, true);
        echo "\"></script>
    ";
        // line 97
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, base_url("vendor/blizzcms/amarantjs/js/jquery.amaran.min.js"), "html", null, true);
        echo "\"></script>
    ";
        // line 99
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, base_url("vendor/blizzcms/pace/js/pace.min.js"), "html", null, true);
        echo "\"></script>
    ";
        // line 101
        echo "    <script>
        window.addEventListener(\"load\", function(){
            window.cookieconsent.initialise({
                \"palette\": {
                    \"popup\": {
                        \"background\": \"#252e39\"
                    },
                    \"button\": {
                        \"background\": \"#1e87f0\"
                    }
                },
                \"theme\": \"edgeless\",
                \"position\": \"bottom-right\",
                \"content\": {
                    \"message\": \"This website uses cookies to ensure you get the best experience on our website. \",
                    \"dismiss\": \"Got it!\",
                    \"link\": \"Learn more\",
                    \"href\": \"";
        // line 118
        echo twig_escape_filter($this->env, base_url("cookies"), "html", null, true);
        echo "\"
                }
            })});
    </script>
</body>
</html>
";
    }

    // line 79
    public function block_content($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "general/layout.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  212 => 79,  201 => 118,  182 => 101,  177 => 99,  172 => 97,  167 => 95,  151 => 80,  149 => 79,  135 => 67,  95 => 30,  90 => 28,  86 => 26,  81 => 25,  76 => 23,  72 => 21,  68 => 20,  63 => 19,  58 => 17,  53 => 15,  49 => 13,  45 => 12,  40 => 11,  35 => 9,  31 => 7,  26 => 5,  20 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
<head>
    <meta charset=\"UTF-8\">
    <title>{{ title }}</title>
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ base_url('assets/images/favicon.ico') }}\">
    {# RPG Awesine #}
    <link rel=\"stylesheet\" href=\"{{ base_url('node_modules/rpg-awesome/css/rpg-awesome.min.css') }}\" />
    {# Uikit #}
    <link rel=\"stylesheet\" href=\"{{ base_url('node_modules/uikit/dist/css/uikit.min.css') }}\" />
    <script src=\"{{ base_url('node_modules/uikit/dist/js/uikit.min.js') }}\"></script>
    <script src=\"{{ base_url('node_modules/uikit/dist/js/uikit-icons.min.js') }}\"></script>
    {# BlizzCMS #}
    <link rel=\"stylesheet\" href=\"{{ base_url('vendor/blizzcms/fxgeneral/css/cms.css') }}\" />
    {# Jquery #}
    <script src=\"{{ base_url('node_modules/jquery/dist/jquery.min.js') }}\"></script>
    {# VueJS #}
    <script src=\"{{ base_url('node_modules/vue/dist/vue.js') }}\"></script>
    <script src=\"{{ base_url('node_modules/vue-resource/dist/vue-resource.min.js') }}\"></script>
\t<script src=\"{{ base_url('node_modules/vue-moment/vue-moment.js') }}\"></script>
    {# Cookieconsent #}
    <link rel=\"stylesheet\" href=\"{{ base_url('node_modules/cookieconsent/build/cookieconsent.min.css') }}\" />
    {# AmaranJs #}
    <link rel=\"stylesheet\" href=\"{{ base_url('vendor/blizzcms/amarantjs/css/amaran.min.css') }}\" />
    <link rel=\"stylesheet\" href=\"{{ base_url('vendor/blizzcms/amarantjs/css/animate.min.css') }}\" />
    {# Awesome #}
    <link rel=\"stylesheet\" href=\"{{ base_url('vendor/blizzcms/font-awesome/css/fontawesome-all.min.css') }}\" />
    {# Pace #}
    <link rel=\"stylesheet\" href=\"{{ base_url('vendor/blizzcms/pace/css/pace-theme-minimal.tmpl.css') }}\" />
</head>
<body>

    <nav class=\"uk-navbar-container uk-background-primary\">
        <div class=\"uk-container\">
            <div class=\"uk-navbar\" uk-navbar=\"mode: click\">
                <div class=\"uk-navbar-left\">
                    <a class=\"uk-navbar-item uk-logo\" href=\"\">BlizzCMSv2</a>
                </div>
                <div class=\"uk-navbar-center\">
                    <ul class=\"uk-navbar-nav uk-visible@m\">
                        <li>
                            <a href=\"javascript:void(0)\">Menu<span uk-icon=\"icon: chevron-down\"></span></a>
                            <div class=\"uk-navbar-dropdown\">
                                <ul class=\"uk-nav uk-navbar-dropdown-nav\">
                                    <li><a href=\"#\">Item</a></li>
                                    <li><a href=\"#\">Item</a></li>
                                    <li><a href=\"#\">Item</a></li>
                                </ul>
                            </div>
                        </li>
                        <li><a href=\"#\">Forum</a></li>
                        <li><a href=\"#\">Store</a></li>
                    </ul>
                </div>
                <div class=\"uk-navbar-right\">
                    <div class=\"uk-navbar-item uk-visible@m\">
                        <ul class=\"uk-grid-small\" uk-grid>
                            <li>
                                <a href=\"\" class=\"uk-button uk-button-primary\"><span uk-icon=\"icon: sign-in\"></span> Sign In</a>
                            </li>
                        </ul>
                    </div>
                    <a class=\"uk-navbar-toggle uk-hidden@m\" uk-navbar-toggle-icon href=\"#mobile\" uk-toggle></a>
                </div>
                {# Mobile Menu #}
                <div class=\"uk-offcanvas-content\">
                    <div id=\"mobile\" uk-offcanvas=\"flip: true\">
                        <div class=\"uk-offcanvas-bar\">
                            <div class=\"uk-panel\">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>

    {% block content %}{% endblock %}

    <div class=\"uk-section uk-section-small\">
        <div class=\"uk-container\">
            <div class=\"uk-grid-large uk-flex-middle uk-grid\" uk-grid>
                <div class=\"uk-width-expand uk-first-column\">
                    <p class=\"uk-align-left\"><a target=\"_blank\" href=\"https://gitlab.com/BlizzCMS\" class=\"uk-button uk-button-link\"><i class=\"fab fa-gitlab fa-2x\"></i></a> Proudly powered by <span class=\"uk-text-bold\">BlizzCMS</span></p>
                </div>
                <div class=\"uk-width-auto\">
                    <p>Copyright &copy; 2018 <span class=\"uk-text-bold\">BlizzCMSv2</span>. All Right Reserved.</p>
                </div>
            </div>
        </div>
    </div>

    {# Cookieconsent #}
    <script src=\"{{ base_url('node_modules/cookieconsent/build/cookieconsent.min.js') }}\"></script>
    {# AmaranJS#}
    <script src=\"{{ base_url('vendor/blizzcms/amarantjs/js/jquery.amaran.min.js') }}\"></script>
    {# Pace #}
    <script src=\"{{ base_url('vendor/blizzcms/pace/js/pace.min.js') }}\"></script>
    {# scripts #}
    <script>
        window.addEventListener(\"load\", function(){
            window.cookieconsent.initialise({
                \"palette\": {
                    \"popup\": {
                        \"background\": \"#252e39\"
                    },
                    \"button\": {
                        \"background\": \"#1e87f0\"
                    }
                },
                \"theme\": \"edgeless\",
                \"position\": \"bottom-right\",
                \"content\": {
                    \"message\": \"This website uses cookies to ensure you get the best experience on our website. \",
                    \"dismiss\": \"Got it!\",
                    \"link\": \"Learn more\",
                    \"href\": \"{{ base_url('cookies') }}\"
                }
            })});
    </script>
</body>
</html>
", "general/layout.twig", "C:\\xampp\\htdocs\\BlizzCMSv2\\application\\views\\general\\layout.twig");
    }
}
