<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
if(!defined('ProjectName')) exit('Code Altered');
/**
 * BlizzCMS
 *
 * An CMS adaptable to any game version "World of Warcraft"
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2018, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	BlizzCMS
 * @author	FixCore
 * @copyright	Copyright (c) 2015 - 2018, FixCore.
 * @copyright	Copyright (c) 2017 - 2018, ProjectCMS (http://projectcms.net/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://blizzcms.projectcms.net/
 * @since	Version 2.0.0
 * @filesource
 */

class Admin extends MX_Controller{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin_model');
	}

	public function index()
	{
		$data = array(
			//require
			'layout' => "general/admin_layout.twig",
			'title' => 'Admin Panel',
		);

		echo $this->twig->render('index', $data);
	}

	public function accounts()
	{
		$data = array(
			//require
			'layout' => "general/admin_layout.twig",
			'title' => 'Manage Accounts',
		);

		echo $this->twig->render('manage_accounts', $data);
	}

	public function lookaccount()
	{
		$data = array(
			//require
			'layout' => "general/admin_layout.twig",
			'title' => 'Account',
		);

		echo $this->twig->render('look_account', $data);
	}

	public function characters()
	{
		$data = array(
			//require
			'layout' => "general/admin_layout.twig",
			'title' => 'Manage Characters',
		);

		echo $this->twig->render('manage_characters', $data);
	}

	public function lookcharacter()
	{
		$data = array(
			//require
			'layout' => "general/admin_layout.twig",
			'title' => 'Character',
		);

		echo $this->twig->render('look_character', $data);
	}

	public function settings()
	{
		$data = array(
			//require
			'layout' => "general/admin_layout.twig",
			'title' => 'Manage Settings',
		);

		echo $this->twig->render('manage_settings', $data);
	}

	public function realms()
	{
		$data = array(
			//require
			'layout' => "general/admin_layout.twig",
			'title' => 'Manage Realms',
		);

		echo $this->twig->render('manage_realms', $data);
	}

	public function slides()
	{
		$data = array(
			//require
			'layout' => "general/admin_layout.twig",
			'title' => 'Manage Slides',
		);

		echo $this->twig->render('manage_slides', $data);
	}

	public function news()
	{
		$data = array(
			//require
			'layout' => "general/admin_layout.twig",
			'title' => 'Manage News',
		);

		echo $this->twig->render('manage_news', $data);
	}

	public function editnews()
	{
		$data = array(
			//require
			'layout' => "general/admin_layout.twig",
			'title' => 'Edit News',
		);

		echo $this->twig->render('edit_news', $data);
	}

	public function changelog()
	{
		$data = array(
			//require
			'layout' => "general/admin_layout.twig",
			'title' => 'Manage Changelog',
		);

		echo $this->twig->render('manage_changelog', $data);
	}

	public function editchangelog()
	{
		$data = array(
			//require
			'layout' => "general/admin_layout.twig",
			'title' => 'Edit Changelog',
		);

		echo $this->twig->render('edit_changelog', $data);
	}

	public function pages()
	{
		$data = array(
			//require
			'layout' => "general/admin_layout.twig",
			'title' => 'Manage Pages',
		);

		echo $this->twig->render('manage_pages', $data);
	}

	public function editpage()
	{
		$data = array(
			//require
			'layout' => "general/admin_layout.twig",
			'title' => 'Edit Page',
		);

		echo $this->twig->render('edit_page', $data);
	}

	public function donations()
	{
		$data = array(
			//require
			'layout' => "general/admin_layout.twig",
			'title' => 'Manage Slides',
		);

		echo $this->twig->render('manage_donations', $data);
	}

	public function categories()
	{
		$data = array(
			//require
			'layout' => "general/admin_layout.twig",
			'title' => 'Manage Categories',
		);

		echo $this->twig->render('manage_categories', $data);
	}

	public function forums()
	{
		$data = array(
			//require
			'layout' => "general/admin_layout.twig",
			'title' => 'Manage Forums',
		);

		echo $this->twig->render('manage_forums', $data);
	}

	public function groups()
	{
		$data = array(
			//require
			'layout' => "general/admin_layout.twig",
			'title' => 'Manage Groups',
		);

		echo $this->twig->render('manage_groups', $data);
	}

	public function items()
	{
		$data = array(
			//require
			'layout' => "general/admin_layout.twig",
			'title' => 'Manage Items',
		);

		echo $this->twig->render('manage_items', $data);
	}

	public function edititem()
	{
		$data = array(
			//require
			'layout' => "general/admin_layout.twig",
			'title' => 'Edit Item',
		);

		echo $this->twig->render('edit_item', $data);
	}

	public function api()
	{
		$data = array(
			//require
			'layout' => "general/admin_layout.twig",
			'title' => 'Manage API',
		);

		echo $this->twig->render('manage_api', $data);
	}
}
