<?php
// Home Lang
$lang['read_more'] = 'Read More';
$lang['status'] = 'Server Status';
$lang['realmlist'] = 'Our realmlist';

//Server
$lang['sv_privacy'] = 'Privacy Policy';
$lang['sv_legal'] = 'Legal Documentation';

//General
$lang['news'] = 'News';
$lang['faq'] = 'Faq';
$lang['arena'] = 'Arena';
$lang['donate'] = 'Donate';
$lang['pages'] = 'Pages';
$lang['pvp'] = 'Pvp Statistics';
$lang['vote'] = 'Vote';
$lang['home'] = 'Home';

//Date
$lang['month_January'] = 'January';
$lang['month_February'] = 'February';
$lang['month_March'] = 'March';
$lang['month_April'] = 'April';
$lang['month_May'] = 'May';
$lang['month_June'] = 'June';
$lang['month_July'] = 'July';
$lang['month_August'] = 'August';
$lang['month_September'] = 'September';
$lang['month_October'] = 'October';
$lang['month_November'] = 'November';
$lang['month_December'] = 'December';

//Races
$lang['race_human'] = 'Human';
$lang['race_orc'] = 'Orc';
$lang['race_dwarf'] = 'Dwarf';
$lang['race_night_elf'] = 'Night Elf';
$lang['race_undead'] = 'Undead';
$lang['race_tauren'] = 'Tauren';
$lang['race_gnome'] = 'Gnome';
$lang['race_troll'] = 'Troll';
$lang['race_goblin'] = 'Goblin';
$lang['race_blood_elf'] = 'Blood Elf';
$lang['race_draenei'] = 'Draenei';
$lang['race_worgen'] = 'Worgen';
$lang['race_panda_neutral'] = 'Panda Neutral';
$lang['race_panda_alli'] = 'Panda Alliance';
$lang['race_panda_horde'] = 'Panda Horde';

//Class
$lang['class_warrior'] = 'Warrior';
$lang['class_paladin'] = 'Paladin';
$lang['class_hunter'] = 'Hunter';
$lang['class_rogue'] = 'Rogue';
$lang['class_priest'] = 'Priest';
$lang['class_dk'] = 'Death Knight';
$lang['class_shamman'] = 'Shamman';
$lang['class_mage'] = 'Mage';
$lang['class_warlock'] = 'Warlock';
$lang['class_monk'] = 'Monk';
$lang['class_druid'] = 'Druid';
$lang['class_demonhunter'] = 'Demon Hunter';

//Gender
$lang['gender_male'] = 'Male';
$lang['gender_female'] = 'Female';
